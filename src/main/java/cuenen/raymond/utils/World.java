package cuenen.raymond.utils;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class World extends BorderPane {

    private final ObservableList<Area> areas = FXCollections.observableArrayList();
    private final Group areaGroup = new Group();
    private final Group axisGroup = new Group();
    private final GridPane controls = new GridPane();
    private int currentRow = 0;

    public World() {
        setLeft(controls);
        Group root = new Group();
        root.setManaged(false);
        root.getChildren().addAll(areaGroup, axisGroup);
        setCenter(root);
        controls.setVgap(10);
        controls.setPadding(new Insets(5, 0, 5, 5));
    }

    public void addArea(Area area, String name) {
        Label label = new Label(name);
        label.setFont(Font.font("System", FontWeight.BOLD, 12));
        GridPane.setConstraints(label, 0, currentRow);
        controls.getChildren().add(label);

        final CheckBox circularCheckBox = new CheckBox("Circular");
        circularCheckBox.setSelected(area.isCircular());
        final Slider distanceASlider = new Slider(area.getDistanceB(), 300, area.getDistanceA());
        final Slider distanceBSlider = new Slider(5, area.getDistanceA(), area.getDistanceB());
        distanceBSlider.maxProperty().bind(distanceASlider.valueProperty());
        distanceASlider.minProperty().bind(distanceBSlider.valueProperty());
        final Slider angleSlider = new Slider(0, 360, area.getAngle());
        final Label distanceAValue = new Label(String.format("%.1f", area.getDistanceA()));
        final Label distanceBValue = new Label(String.format("%.1f", area.getDistanceB()));
        final Label angleValue = new Label(String.format("%.1f", area.getAngle()));

        area.circularProperty().bind(circularCheckBox.selectedProperty());
        GridPane.setConstraints(circularCheckBox, 1, currentRow++);
        GridPane.setColumnSpan(circularCheckBox, 2);
        controls.getChildren().add(circularCheckBox);

        label = new Label("Distance A");
        GridPane.setConstraints(label, 0, currentRow);
        controls.getChildren().add(label);

        area.distanceAProperty().bind(distanceASlider.valueProperty());
        GridPane.setConstraints(distanceASlider, 1, currentRow);
        controls.getChildren().add(distanceASlider);

        distanceASlider.valueProperty().addListener((ov, oldVal, newVal) -> {
            distanceAValue.setText(String.format("%.1f", newVal));
            distanceBSlider.setDisable(distanceBSlider.getValue() == distanceBSlider.getMin()
                    && newVal.doubleValue() == distanceASlider.getMin());
        });
        GridPane.setConstraints(distanceAValue, 2, currentRow++);
        controls.getChildren().add(distanceAValue);

        label = new Label("Distance B");
        GridPane.setConstraints(label, 0, currentRow);
        controls.getChildren().add(label);

        area.distanceBProperty().bind(distanceBSlider.valueProperty());
        GridPane.setConstraints(distanceBSlider, 1, currentRow);
        controls.getChildren().add(distanceBSlider);

        distanceBSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
            distanceBValue.setText(String.format("%.1f", newVal));
            distanceASlider.setDisable(distanceASlider.getValue() == distanceASlider.getMax()
                    && newVal.doubleValue() == distanceBSlider.getMax());

        });
        GridPane.setConstraints(distanceBValue, 2, currentRow++);
        controls.getChildren().add(distanceBValue);

        label = new Label("Angle");
        GridPane.setConstraints(label, 0, currentRow);
        controls.getChildren().add(label);

        area.angleProperty().bind(angleSlider.valueProperty());
        GridPane.setConstraints(angleSlider, 1, currentRow);
        controls.getChildren().add(angleSlider);

        angleSlider.valueProperty().addListener((ov, oldVal, newVal) -> angleValue.setText(String.format("%.1f", newVal)));
        GridPane.setConstraints(angleValue, 2, currentRow++);
        controls.getChildren().add(angleValue);

        Separator sep = new Separator(Orientation.HORIZONTAL);
        GridPane.setConstraints(sep, 0, currentRow++);
        GridPane.setColumnSpan(sep, 3);
        controls.getChildren().add(sep);

        SeperatingAxis axis = new SeperatingAxis(area, areas);
        axis.sizeProperty().bind(Bindings.max(widthProperty(), heightProperty()));

        areas.add(area);
        areaGroup.getChildren().add(area);
        axisGroup.getChildren().add(axis);
    }
}
