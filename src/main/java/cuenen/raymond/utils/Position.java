package cuenen.raymond.utils;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class Position {

    public static final Position ORIGIN = new Position(0, 0);

    private final DoubleProperty xProperty;
    private final DoubleProperty yProperty;

    public Position(double x, double y) {
        xProperty = new SimpleDoubleProperty(x);
        yProperty = new SimpleDoubleProperty(y);
    }

    public final double getX() {
        return xProperty.get();
    }

    public final void setX(double x) {
        xProperty.set(x);
    }

    public DoubleProperty xProperty() {
        return xProperty;
    }

    public final double getY() {
        return yProperty.get();
    }

    public final void setY(double y) {
        yProperty.set(y);
    }

    public DoubleProperty yProperty() {
        return yProperty;
    }

    public Position rotate(double angle) {
        return rotate(Position.ORIGIN, angle);
    }

    public Position rotate(Position center, double angle) {
        double s = Math.sin(angle);
        double c = Math.cos(angle);
        double x = this.getX() - center.getX();
        double y = this.getY() - center.getY();
        double rx = x * c - y * s;
        double ry = x * s + y * c;
        return new Position(rx + center.getX(), ry + center.getY());
    }

    public Position translate(double dx, double dy) {
        return new Position(getX() + dx, getY() + dy);
    }

    @Override
    public String toString() {
        return String.format("Position(%.7f, %.7f)", getX(), getY());
    }
}
