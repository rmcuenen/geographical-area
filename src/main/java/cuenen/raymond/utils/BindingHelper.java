package cuenen.raymond.utils;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.NumberBinding;
import javafx.beans.value.ObservableNumberValue;
import javafx.geometry.Point2D;

public final class BindingHelper {

    public static enum ReturnType {

        valueX, valueY;
    }

    public static NumberBinding rotateProperty(
            final ObservableNumberValue center,
            final ObservableNumberValue angle,
            final ObservableNumberValue distance,
            final ReturnType returnType) {
        return new DoubleBinding() {
            {
                super.bind(center, angle, distance);
            }

            @Override
            protected double computeValue() {
                double factor = returnType == ReturnType.valueX ? Math.cos(Math.toRadians(angle.doubleValue()))
                        : Math.sin(Math.toRadians(angle.doubleValue()));
                return center.doubleValue() + distance.doubleValue() * factor;
            }
        };
    }

    public static NumberBinding distanceProperty(final ObservableNumberValue point1, final ObservableNumberValue point2) {
        return new DoubleBinding() {
            {
                super.bind(point1, point2);
            }

            @Override
            protected double computeValue() {
                return Math.sqrt(Math.pow(point1.doubleValue(), 2) + Math.pow(point2.doubleValue(), 2));
            }
        };
    }

    public static NumberBinding angleProperty(final ObservableNumberValue x, final ObservableNumberValue y) {
        return new DoubleBinding() {
            {
                super.bind(x, y);
            }

            @Override
            protected double computeValue() {
                return Math.toDegrees(Math.atan2(y.doubleValue(), x.doubleValue()));
            }
        };
    }

    public static NumberBinding normalProperty(final ObservableNumberValue deltaX, final ObservableNumberValue deltaY, final ReturnType returnType) {
        return new DoubleBinding() {
            {
                super.bind(deltaX, deltaY);
            }

            @Override
            protected double computeValue() {
                Point2D axis = new Point2D(deltaX.doubleValue(), deltaY.doubleValue()).normalize();
                return returnType == ReturnType.valueX ? axis.getX() : axis.getY();
            }
        };
    }

    public static NumberBinding shortestDistance(final ObservableNumberValue centerX, final ObservableNumberValue centerY, final Area area, final ReturnType returnType) {
        return new DoubleBinding() {
            {
                super.bind(area.centerXProperty(), area.centerYProperty(), area.distanceAProperty(), area.distanceBProperty(),
                        area.angleProperty(), area.circularProperty(), centerX, centerY);
            }

            @Override
            protected double computeValue() {
                double rho = Math.sqrt(Math.pow(area.getDistanceA(), 2) + Math.pow(area.getDistanceB(), 2));
                double phi = Math.atan2(area.getDistanceB(), area.getDistanceA());
                Point2D point = new Point2D(area.getCenterX(), area.getCenterY());
                if (!area.isCircular()) {
                    double d = Double.POSITIVE_INFINITY;
                    double theta = Math.toRadians(area.getAngle());
                    double[] angles = {-phi - Math.PI / 2, phi - Math.PI / 2, Math.PI / 2 - phi, Math.PI / 2 + phi};
                    for (double angle : angles) {
                        double x = area.getCenterX() + rho * Math.cos(theta + angle);
                        double y = area.getCenterY() + rho * Math.sin(theta + angle);
                        double distance = Math.sqrt(Math.pow(x - centerX.doubleValue(), 2) + Math.pow(y - centerY.doubleValue(), 2));
                        if (distance < d) {
                            d = distance;
                            point = new Point2D(x, y);
                        }
                    }
                }
                return returnType == ReturnType.valueX ? point.getX() : point.getY();
            }
        };
    }

    private BindingHelper() {

    }

}
