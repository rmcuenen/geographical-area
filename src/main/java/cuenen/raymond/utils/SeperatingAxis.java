package cuenen.raymond.utils;

import cuenen.raymond.utils.BindingHelper.ReturnType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.paint.Color;

public class SeperatingAxis extends Group {

    private final DoubleProperty sizeProperty = new SimpleDoubleProperty(1024);
    private final Map<Area, Projection> centerAxes = new HashMap<>();
    private final Group center = new Group();
    private final Area origin;

    public SeperatingAxis(Area origin, ObservableList<Area> targets) {
        this.origin = origin;
        createAxis(targets);
        targets.addListener((Change<? extends Area> c) -> centerAxis(c.getList()));
    }

    private void createAxis(ObservableList<Area> targets) {
        Projection longAxis = new Projection(targets, false);
        longAxis.startXProperty().bind(BindingHelper.rotateProperty(origin.centerXProperty(), origin.angleProperty().add(90), sizeProperty.multiply(2), ReturnType.valueX));
        longAxis.startYProperty().bind(BindingHelper.rotateProperty(origin.centerYProperty(), origin.angleProperty().add(90), sizeProperty.multiply(2), ReturnType.valueY));
        longAxis.endXProperty().bind(BindingHelper.rotateProperty(origin.centerXProperty(), origin.angleProperty().subtract(90), sizeProperty.multiply(2), ReturnType.valueX));
        longAxis.endYProperty().bind(BindingHelper.rotateProperty(origin.centerYProperty(), origin.angleProperty().subtract(90), sizeProperty.multiply(2), ReturnType.valueY));
        longAxis.setStroke(Color.YELLOW);
        longAxis.visibleProperty().bind(origin.circularProperty().not());
        Projection shortAxis = new Projection(targets, false);
        shortAxis.startXProperty().bind(BindingHelper.rotateProperty(origin.centerXProperty(), origin.angleProperty().add(180), sizeProperty.multiply(2), ReturnType.valueX));
        shortAxis.startYProperty().bind(BindingHelper.rotateProperty(origin.centerYProperty(), origin.angleProperty().add(180), sizeProperty.multiply(2), ReturnType.valueY));
        shortAxis.endXProperty().bind(BindingHelper.rotateProperty(origin.centerXProperty(), origin.angleProperty(), sizeProperty.multiply(2), ReturnType.valueX));
        shortAxis.endYProperty().bind(BindingHelper.rotateProperty(origin.centerYProperty(), origin.angleProperty(), sizeProperty.multiply(2), ReturnType.valueY));
        shortAxis.setStroke(Color.YELLOW);
        shortAxis.visibleProperty().bind(origin.circularProperty().not());
        getChildren().addAll(center, longAxis, shortAxis);
        centerAxis(targets);
    }

    private void centerAxis(final ObservableList<? extends Area> targets) {
        final List<Area> toRemove = new ArrayList<>(centerAxes.keySet());
        targets.stream().forEach((area) -> {
            if (!toRemove.remove(area)) {
                NumberBinding dx = Bindings.subtract(
                        BindingHelper.shortestDistance(origin.centerXProperty(), origin.centerYProperty(), area, ReturnType.valueX),
                        origin.centerXProperty()).multiply(sizeProperty.multiply(2));
                NumberBinding dy = Bindings.subtract(
                        BindingHelper.shortestDistance(origin.centerXProperty(), origin.centerYProperty(), area, ReturnType.valueY),
                        origin.centerYProperty()).multiply(sizeProperty.multiply(2));
                Projection centerAxis = new Projection(targets, true);
                centerAxis.startXProperty().bind(origin.centerXProperty().subtract(dx));
                centerAxis.startYProperty().bind(origin.centerYProperty().subtract(dy));
                centerAxis.endXProperty().bind(origin.centerXProperty().add(dx));
                centerAxis.endYProperty().bind(origin.centerYProperty().add(dy));
                centerAxis.setStroke(Color.YELLOW);
                centerAxis.visibleProperty().bind(origin.circularProperty());
                center.getChildren().add(centerAxis);
                centerAxes.put(area, centerAxis);
            }
        });
        toRemove.stream().map((area) -> centerAxes.remove(area)).forEach((axis) -> axis.unbind());
    }

    public final double getSize() {
        return sizeProperty.get();
    }

    public final void setSize(double size) {
        sizeProperty.set(size);
    }

    public DoubleProperty sizeProperty() {
        return sizeProperty;
    }
}
