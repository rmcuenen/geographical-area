package cuenen.raymond.utils;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ShapeViewer extends Application {

    private void init(Stage primaryStage) {
        Group root = new Group();
        primaryStage.setScene(new Scene(root, 1024, 720));

        World world = new World();
        world.setPrefSize(1024, 720);

        Area ellipse = new Area(512, 360, 250, 150, 30, true);
        ellipse.setFill(Color.GREY);
        ellipse.setStroke(Color.RED.deriveColor(0, 1, 1, 0.6));
        world.addArea(ellipse, "Shape 1");

        double t = Math.toRadians(-60);
        Area rectangle = new Area(512 + 125 * Math.cos(t), 360 + 125 * Math.sin(t), 250, 50, 120);
        rectangle.setFill(Color.LIGHTGREY);
        rectangle.setStroke(Color.BLUE.deriveColor(0, 1, 1, 0.6));
        world.addArea(rectangle, "Shape 2");

        root.getChildren().add(world);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        init(primaryStage);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
