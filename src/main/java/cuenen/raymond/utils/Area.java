package cuenen.raymond.utils;

import cuenen.raymond.utils.BindingHelper.ReturnType;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.ArcTo;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;

public class Area extends Path {

    private final DoubleProperty centerXProperty;
    private final DoubleProperty centerYProperty;
    private final DoubleProperty distanceAProperty;
    private final DoubleProperty distanceBProperty;
    private final DoubleProperty angleProperty;
    private final BooleanProperty circularProperty;

    private Point2D initialPoint = Point2D.ZERO;
    private Point2D grabPoint = Point2D.ZERO;

    public Area(double cx, double cy, double distanceA) {
        this(cx, cy, distanceA, distanceA, 0, true);
    }

    public Area(double cx, double cy, double distanceA, double angle) {
        this(cx, cy, distanceA, distanceA, angle, false);
    }

    public Area(double cx, double cy, double distanceA, double distanceB, double angle) {
        this(cx, cy, distanceA, distanceB, angle, false);
    }

    public Area(double cx, double cy, double distanceA, double distanceB, double angle, boolean circular) {
        circularProperty = new SimpleBooleanProperty(circular);
        distanceAProperty = new SimpleDoubleProperty(distanceA);
        distanceBProperty = new SimpleDoubleProperty(distanceB);
        angleProperty = new SimpleDoubleProperty(angle);
        centerXProperty = new SimpleDoubleProperty(cx);
        centerYProperty = new SimpleDoubleProperty(cy);
        setCursor(Cursor.HAND);
        setStrokeWidth(0);
        createPath();
        initListeners();
    }

    private void initListeners() {
        circularProperty.addListener((ov, o, n) -> createPath());
        setOnMousePressed((MouseEvent me) -> {
            initialPoint = new Point2D(getCenterX(), getCenterY());
            grabPoint = new Point2D(me.getSceneX(), me.getSceneY());
            Group parent = ((Group) getParent());
            parent.getChildren().remove(this);
            parent.getChildren().add(this);
        });
        setOnMouseDragged((MouseEvent me) -> {
            double posX = initialPoint.getX() + me.getSceneX() - grabPoint.getX();
            double posY = initialPoint.getY() + me.getSceneY() - grabPoint.getY();
            setCenterX(posX);
            setCenterY(posY);
        });
    }

    private void createPath() {
        ObservableList<PathElement> path = getElements();
        path.clear();
        if (circularProperty.get()) {
            NumberBinding a = Bindings.max(distanceAProperty, distanceBProperty);
            NumberBinding b = Bindings.min(distanceAProperty, distanceBProperty);
            NumberBinding startX = BindingHelper.rotateProperty(centerXProperty, angleProperty.subtract(90), a, ReturnType.valueX);
            NumberBinding startY = BindingHelper.rotateProperty(centerYProperty, angleProperty.subtract(90), a, ReturnType.valueY);
            NumberBinding endX = BindingHelper.rotateProperty(centerXProperty, angleProperty.add(90), a, ReturnType.valueX);
            NumberBinding endY = BindingHelper.rotateProperty(centerYProperty, angleProperty.add(90), a, ReturnType.valueY);
            MoveTo move = new MoveTo();
            move.xProperty().bind(startX);
            move.yProperty().bind(startY);
            ArcTo arc1 = new ArcTo();
            arc1.radiusXProperty().bind(a);
            arc1.radiusYProperty().bind(b);
            arc1.XAxisRotationProperty().bind(angleProperty.subtract(90));
            arc1.xProperty().bind(endX);
            arc1.yProperty().bind(endY);
            arc1.setLargeArcFlag(true);
            arc1.setSweepFlag(true);
            ArcTo arc2 = new ArcTo();
            arc2.radiusXProperty().bind(a);
            arc2.radiusYProperty().bind(b);
            arc2.XAxisRotationProperty().bind(angleProperty.subtract(90));
            arc2.xProperty().bind(startX);
            arc2.yProperty().bind(startY);
            arc2.setLargeArcFlag(true);
            arc2.setSweepFlag(true);
            path.addAll(move, arc1, arc2);
        } else {
            NumberBinding rho = BindingHelper.distanceProperty(distanceAProperty, distanceBProperty);
            NumberBinding phi = BindingHelper.angleProperty(distanceAProperty, distanceBProperty);
            MoveTo move = new MoveTo();
            move.xProperty().bind(BindingHelper.rotateProperty(centerXProperty, angleProperty.subtract(90).subtract(phi), rho, ReturnType.valueX));
            move.yProperty().bind(BindingHelper.rotateProperty(centerYProperty, angleProperty.subtract(90).subtract(phi), rho, ReturnType.valueY));
            LineTo line1 = new LineTo();
            line1.xProperty().bind(BindingHelper.rotateProperty(centerXProperty, angleProperty.subtract(90).add(phi), rho, ReturnType.valueX));
            line1.yProperty().bind(BindingHelper.rotateProperty(centerYProperty, angleProperty.subtract(90).add(phi), rho, ReturnType.valueY));
            LineTo line2 = new LineTo();
            line2.xProperty().bind(BindingHelper.rotateProperty(centerXProperty, angleProperty.add(90).subtract(phi), rho, ReturnType.valueX));
            line2.yProperty().bind(BindingHelper.rotateProperty(centerYProperty, angleProperty.add(90).subtract(phi), rho, ReturnType.valueY));
            LineTo line3 = new LineTo();
            line3.xProperty().bind(BindingHelper.rotateProperty(centerXProperty, angleProperty.add(90).add(phi), rho, ReturnType.valueX));
            line3.yProperty().bind(BindingHelper.rotateProperty(centerYProperty, angleProperty.add(90).add(phi), rho, ReturnType.valueY));
            path.addAll(move, line1, line2, line3, new ClosePath());
        }
    }

    public final double getCenterX() {
        return centerXProperty.get();
    }

    public final void setCenterX(double x) {
        centerXProperty.set(x);
    }

    public DoubleProperty centerXProperty() {
        return centerXProperty;
    }

    public final double getCenterY() {
        return centerYProperty.get();
    }

    public final void setCenterY(double y) {
        centerYProperty.set(y);
    }

    public DoubleProperty centerYProperty() {
        return centerYProperty;
    }

    public final double getDistanceA() {
        return distanceAProperty.get();
    }

    public final void setDistanceA(double distanceA) {
        distanceAProperty.set(distanceA);
    }

    public DoubleProperty distanceAProperty() {
        return distanceAProperty;
    }

    public final double getDistanceB() {
        return distanceBProperty.get();
    }

    public final void setDistanceB(double distanceB) {
        distanceBProperty.set(distanceB);
    }

    public DoubleProperty distanceBProperty() {
        return distanceBProperty;
    }

    public final double getAngle() {
        return angleProperty.get();
    }

    public final void setAngle(double angle) {
        angleProperty.set(angle);
    }

    public DoubleProperty angleProperty() {
        return angleProperty;
    }

    public final boolean isCircular() {
        return circularProperty.get();
    }

    public final void setCircular(boolean circular) {
        circularProperty.set(circular);
    }

    public BooleanProperty circularProperty() {
        return circularProperty;
    }
}
