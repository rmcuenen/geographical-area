package cuenen.raymond.utils;

import cuenen.raymond.utils.BindingHelper.ReturnType;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;

public class Projection extends Group {

    private final DoubleProperty centerXProperty = new SimpleDoubleProperty();
    private final DoubleProperty centerYProperty = new SimpleDoubleProperty();
    private final DoubleProperty deltaXProperty = new SimpleDoubleProperty();
    private final DoubleProperty deltaYProperty = new SimpleDoubleProperty();
    private final Line axis = new Line();

    public Projection(ObservableList<? extends Area> areas, boolean center) {
        centerXProperty.bind(axis.startXProperty().add(Bindings.subtract(axis.endXProperty(), axis.startXProperty()).divide(2)));
        centerYProperty.bind(axis.startYProperty().add(Bindings.subtract(axis.endYProperty(), axis.startYProperty()).divide(2)));
        deltaXProperty.bind(Bindings.subtract(axis.endXProperty(), axis.startXProperty()));
        deltaYProperty.bind(Bindings.subtract(axis.endYProperty(), axis.startYProperty()));
        getChildren().add(axis);
        if (center) {
            project(areas);
        } else {
            areas.addListener((Change<? extends Area> c) -> project(c.getList()));
        }
    }

    public void unbind() {
        axis.startXProperty().unbind();
        axis.startYProperty().unbind();
        axis.endXProperty().unbind();
        axis.endYProperty().unbind();
        visibleProperty().unbind();
    }

    public DoubleProperty startXProperty() {
        return axis.startXProperty();
    }

    public DoubleProperty startYProperty() {
        return axis.startYProperty();
    }

    public DoubleProperty endXProperty() {
        return axis.endXProperty();
    }

    public DoubleProperty endYProperty() {
        return axis.endYProperty();
    }

    public void setStroke(Paint paint) {
        axis.setStroke(paint);
    }

    private void project(ObservableList<? extends Area> areas) {
        getChildren().clear();
        getChildren().add(axis);
        areas.stream().forEach((area) -> {
            Line projection = new Line();
            projection.strokeProperty().bind(area.strokeProperty());
            projection.setStrokeWidth(3);
            NumberBinding dx = BindingHelper.normalProperty(deltaXProperty, deltaYProperty, ReturnType.valueX);
            NumberBinding dy = BindingHelper.normalProperty(deltaXProperty, deltaYProperty, ReturnType.valueY);
            projection.startXProperty().bind(centerXProperty.add(dx.multiply(projectionProperty(area, ReturnType.valueX))));
            projection.startYProperty().bind(centerYProperty.add(dy.multiply(projectionProperty(area, ReturnType.valueX))));
            projection.endXProperty().bind(centerXProperty.add(dx.multiply(projectionProperty(area, ReturnType.valueY))));
            projection.endYProperty().bind(centerYProperty.add(dy.multiply(projectionProperty(area, ReturnType.valueY))));
            getChildren().add(projection);
        });
    }

    private NumberBinding projectionProperty(final Area area, final ReturnType returnType) {
        return new DoubleBinding() {
            {
                super.bind(area.centerXProperty(), area.centerYProperty(), area.angleProperty(),
                        area.distanceAProperty(), area.distanceBProperty(), area.circularProperty(),
                        deltaXProperty, deltaYProperty, centerXProperty, centerYProperty);
            }

            @Override
            protected double computeValue() {
                Point2D axis = new Point2D(deltaXProperty.get(), deltaYProperty.get()).normalize();
                if (area.isCircular()) {
                    double s = axis.dotProduct(area.getCenterX() - centerXProperty.get(),
                            area.getCenterY() - centerYProperty.get());
                    Point2D normal = rotate(axis, 90 - area.getAngle());
                    double f = area.getDistanceA() * area.getDistanceB() / Math.sqrt(Math.pow(area.getDistanceA(), 2) * Math.pow(normal.getY(), 2) + Math.pow(area.getDistanceB(), 2) * Math.pow(normal.getX(), 2));
                    double w = normal.multiply(f).magnitude();
                    return returnType == ReturnType.valueX ? s - w : s + w;
                } else {
                    double rho = Math.sqrt(Math.pow(area.getDistanceA(), 2) + Math.pow(area.getDistanceB(), 2));
                    double phi = Math.toDegrees(Math.atan2(area.getDistanceB(), area.getDistanceA()));
                    double[] angles = {-phi - 90, phi - 90, 90 - phi, 90 + phi};
                    double min = Double.POSITIVE_INFINITY;
                    double max = Double.NEGATIVE_INFINITY;
                    for (double angle : angles) {
                        double x = area.getCenterX() - centerXProperty.get() + rho * Math.cos(Math.toRadians(area.getAngle() + angle));
                        double y = area.getCenterY() - centerYProperty.get() + rho * Math.sin(Math.toRadians(area.getAngle() + angle));
                        double p = axis.dotProduct(x, y);
                        if (p < min) {
                            min = p;
                        }
                        if (p > max) {
                            max = p;
                        }
                    }
                    return returnType == ReturnType.valueX ? min : max;
                }

            }
        };
    }

    private Point2D rotate(Point2D point, double angle) {
        double t = Math.toRadians(angle);
        return new Point2D(point.getX() * Math.cos(t) - point.getY() * Math.sin(t),
                point.getX() * Math.sin(t) + point.getY() * Math.cos(t));
    }

    public final double getCenterX() {
        return centerXProperty.get();
    }

    public final void setCenterX(double x) {
        centerXProperty.set(x);
    }

    public DoubleProperty centerXProperty() {
        return centerXProperty;
    }

    public final double getCenterY() {
        return centerYProperty.get();
    }

    public final void setCenterY(double y) {
        centerYProperty.set(y);
    }

    public DoubleProperty centerYProperty() {
        return centerYProperty;
    }

    public final double getDeltaX() {
        return deltaXProperty.get();
    }

    public final void setDeltaX(double dx) {
        deltaXProperty.set(dx);
    }

    public DoubleProperty deltaXProperty() {
        return deltaXProperty;
    }

    public final double getDeltaY() {
        return deltaYProperty.get();
    }

    public final void setDeltaY(double dy) {
        deltaYProperty.set(dy);
    }

    public DoubleProperty deltaYProperty() {
        return deltaYProperty;
    }
}
